/*
 * Copyright (c) 2015
 * Routine JS - Menu
 * Version 0.1.0
 * Create 2015.12.18
 * Author Bunker Labs

 * Usage:
 * 1)add structure
 * 2)add necessarily style
 * 3)set number value attribute 'data-height-menu'


 * Code structure:
 *  <div class="Script-Menu">
 *       <div class="Menu-Button" data-height-menu="6">
 *          <span>Click Here</span>
 *       </div>
 *       <div class="Menu-List">
 *           <ul>
 *               <li>Item 1</li>
 *               <li>Item 2</li>
 *               <li>Item 3</li>
 *               <li>Item 4</li>
 *               <li>Item 5</li>
 *               <li>Item 6</li>
 *               <li>Item 7</li>
 *               <li>Item 8</li>
 *               <li>Item 9</li>
 *               <li>Item 10</li>
 *           </ul>
 *       </div>
 *   </div>


 * Necessarily css
 *
 *   .Menu-List{
 *        display: none;
 *        height: 0;
 *        overflow-y: scroll;
 *        transition: height 0.2s linear;
 *    }
 *

 */


var Width = $(window).width();
//
$(window).load(function () {
    if (Width >= 1200) {
        $(".Phones-Header").removeClass('JS-Menu-List');
        $(".Phones-Header").css({'display': "", "height": ""});
    }else{
        $(".Phones-Header").addClass('JS-Menu-List');
    }


});


$(window).resize(function () {

    Width = $(window).width();

    if (Width >= 1200) {
        $(".Phones-Header").removeClass('JS-Menu-List');
        $(".Phones-Header").css({'display': "", "height": ""});
    }else{
        $(".Phones-Header").addClass('JS-Menu-List');
    }

});

$.fn.Drobdown_Header = function () {
    var $Global = $(this);


    $Global.find('.JS-Menu-Button').click(function () {

        if ($Global.find('.JS-Menu-List').is(':hidden')) {

            var Actual_Height = $Global.find('.JS-Menu-List > div').actual('outerHeight');

            $Global.find('.JS-Menu-List').css({'display': 'block', 'height': Actual_Height});

        } else {
            function Time_Display() {
                $Global.find('.JS-Menu-List').css({'display': 'none'});
            }

            $Global.find('.JS-Menu-List').css({'height': '0'});

            setTimeout(Time_Display, 200);
        }

        return false;
    });

    $('.JS-Menu-List').click(function(){
        return false;
    });


    $('body, html').click(function () {

        $Global.find('.JS-Menu-List').css({'height': '0'});

        function Time_Display_Body() {
            $Global.find('.JS-Menu-List').css({'display': 'none'});
        }

        setTimeout(Time_Display_Body, 200);


    });

};

$(".Header-Wrap").Drobdown_Header();
