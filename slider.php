<html>
<head>
    <meta charset="UTF-8">
    <title>10 minutes Slider</title>
</head>
<body>

<style>
    *
    {
        margin:  0;
        padding: 0;
    }

    body
    {
        background: black;
        margin:   0;
        overflow: hidden;
    }

    .JS-Carousel
    {
        position: absolute;
        width:    100%;
        height:   100%;
        top:      0;
    }

    .JS-Carousel-List
    {
        position: relative;
        overflow: hidden;
        width:    100%;
        height:   100%;
    }

    .JS-Carousel-Item
    {
        position:            absolute;
        top:                 0;
        left:                0;
        width:               100%;
        height:              100%;
        transition:          1s ease;
        background-size:     cover;
        background-position: center;
    }

    .Logo
    {
        position: absolute;
        left: 0;
        right: 0;
        z-index:  10;
        top:     -100px;
        width:    94%;
        left: 3%;
        right: 3%;
    }

</style>

<?php
$Dir = '/slider/02_vals/';




$Images = scandir(__DIR__ . $Dir);
unset($Images[0]);
unset($Images[1]);

?>

<img class="Logo" src="/slider/logo.png" alt="">

<div class="JS-Carousel">
    <ul class="JS-Carousel-List">
        <?php foreach ($Images as $image) { ?>
            <li class="JS-Carousel-Item" style="background-image: url(<?= $Dir . $image ?>)"></li>
        <?php } ?>
    </ul>
</div>

<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="/index.min.js"></script>

<script>
    $('.JS-Carousel').JS_Easy_Carousel({
        Autoscroll: 6000,
        Effect: 'fade',
        Height: false
    });
</script>
</body>
</html>