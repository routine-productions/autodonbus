<!DOCTYPE html>
<html>

<head lang="ru">
    <meta charset="UTF-8">
    <title>Autodonbus - Автобусные перевозки</title>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800italic,800&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/index.min.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/img/fav/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/fav/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/fav/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/fav/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/fav/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/fav/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/fav/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/fav/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/fav/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/img/fav/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/fav/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="/img/fav/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/img/fav/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/img/fav/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/img/fav/manifest.json">
    <link rel="mask-icon" href="/img/fav/safari-pinned-tab.svg" color="#fbb03b">
    <meta name="msapplication-TileColor" content="#00a300">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript"> (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter37221340 = new Ya.Metrika({
                        id: 37221340,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true
                    });
                } catch (e) {
                }
            });
            var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
                n.parentNode.insertBefore(s, n);
            };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks"); </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/37221340" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript> <!-- /Yandex.Metrika counter -->
</head>

<body>

<?php
$Dir_Images = __DIR__ . '/images/';
//?>

<div class="Top-Wrap">
    <div class="Parallax"></div>
    <div class="Top-Wrapper">
        <?php require_once __DIR__ . '/modules/top.php'; ?>

    </div>
</div>

<main>

    <?php
    require __DIR__ . '/modules/directions.php';
    require __DIR__ . '/modules/reviews.php';
    require __DIR__ . '/modules/about-us.php';
    ?>
</main>

<?php require __DIR__ . "/modules/footer-phones.php"; ?>
<div class="Site-Footer">
    <footer>
        <dl class="Copyright">
            <dt>© 2016 Auto-Donbus</dt>
            <dd>
                <span>Пассажирские перевозки</span>
                из Донецка в Россию и обратно
            </dd>
        </dl>
        <div class="Vk">
            <a href="http://vk.com/autodonbus">
                <svg>
                    <use xlink:href="#vk"></use>
                </svg>

                <span>Группа ВКонтакте</span>
            </a>
        </div>
        <div class="Made-By">

            <span>Разработка сайта</span>
            <a href="http://routine.bunker-lab.com/">Routine.Production</a>
        </div>

    </footer>
</div>
<?php require __DIR__ . "/modules/review-pop-up.php"; ?>
<?php require __DIR__ . "/modules/delivery-pop-up.php"; ?>

<?php require __DIR__ . "/img/sprite.svg"; ?>

<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="index.min.js"></script>
<body>


</html>

