<?php
$Data = [
    [
        'alias' => 'Moscow',
        [
            'direction' => 'Донецк-Москва',
            'departure-place' => 'АВ Южный',
            'departure-time' => '17:00',
            'departure-days' => 'Понедельник, четверг',
            'arrival-place' => 'АС Новоясеневская',
            'arrival-time' => '15-00',
            'time' => '22ч',
            'price' => '1800'
        ],
        [
            'direction' => 'Москва-Донецк',
            'departure-place' => 'АВ «Новоясеневская»',
            'departure-time' => '10:30',
            'departure-days' => 'Среда, суббота',
            'arrival-place' => 'АВ Южный',
            'arrival-time' => '8-00',
            'time' => '22ч 30мин',
            'price' => '2500'
        ]
    ],

    [
        'alias' => 'Rostov',
        [
            'direction' => 'Донецк-Ростов',
            'departure-place' => 'АВ Южный',
            'departure-time' => '15:00',
            'departure-days' => 'Ежедневно',
            'arrival-place' => 'АВ г. Ростов',
            'arrival-time' => '21:30',
            'time' => '6ч 30мин',
            'price' => '650'
        ],
        [
            'direction' => 'Ростов-Донецк',
            'departure-place' => 'АВ г. Ростов',
            'departure-time' => '5:00',
            'departure-days' => 'Ежедневно',
            'arrival-place' => 'АВ Южный',
            'arrival-time' => '11:30',
            'time' => '6ч 30мин',
            'price' => '650'
        ]
    ],
    [
        'alias' => 'Voronezh',
        [
            'direction' => 'Донецк-Воронеж',
            'departure-place' => 'АВ Южный',
            'departure-time' => '18:00',
            'departure-days' => 'Вторник, пятница',
            'arrival-place' => 'АВ-1 г. Воронеж',
            'arrival-time' => '7:30',
            'time' => '13ч 30мин',
            'price' => '1400'
        ],
        [
            'direction' => 'Воронеж-Донецк',
            'departure-place' => 'АВ-1 г.Воронеж',
            'departure-time' => '17:00',
            'departure-days' => 'Среда, суббота',
            'arrival-place' => 'АВ Южный',
            'arrival-time' => '8:00',
            'time' => '22ч 3мин',
            'price' => '1400'
        ],
        [
            'direction' => 'Донецк-Воронеж',
            'departure-place' => 'АВ Южный',
            'departure-time' => '18:00',
            'departure-days' => 'Вторник, пятница',
            'arrival-place' => 'АВ-1 г. Воронеж',
            'arrival-time' => '7:30',
            'time' => '13ч 30мин',
            'price' => '1400'
        ],
        [
            'direction' => 'Воронеж-Донецк',
            'departure-place' => 'ЖД Вокзал г.Воронеж',
            'departure-time' => '18:30',
            'departure-days' => 'Четверг, воскресенье',
            'arrival-place' => 'АВ Южный',
            'arrival-time' => '7:00',
            'time' => '12ч 30мин',
            'price' => '1400'
        ],
    ],
    [
        'alias' => 'Tula',
        [
            'direction' => 'Донецк-Тула',
            'departure-place' => 'АВ Южный',
            'departure-time' => '17:00',
            'departure-days' => 'Понедельник, четверг',
            'arrival-place' => 'АВ г.Тула',
            'arrival-time' => '13:00',
            'time' => '20ч',
            'price' => '1800'
        ],
        [
            'direction' => 'Тула-Донецк',
            'departure-place' => 'АВ г. Тула',
            'departure-time' => '13:00',
            'departure-days' => 'Среда, суббота',
            'arrival-place' => 'АВ Южный',
            'arrival-time' => '8:00',
            'time' => '20',
            'price' => '1800'
        ]
    ],
    [
        'alias' => 'Kursk',
        [
            'direction' => 'Донецк-Курск',
            'departure-place' => 'АВ Южный',
            'departure-time' => '18:00',
            'departure-days' => 'Вторник, пятница',
            'arrival-place' => 'АВ г.Курск',
            'arrival-time' => '14:00',
            'time' => '20ч',
            'price' => '1700'
        ],
        [
            'direction' => 'Курск-Донецк',
            'departure-place' => 'АВ г. Курск',
            'departure-time' => '11:00',
            'departure-days' => 'Четверг, воскресенье',
            'arrival-place' => 'АВ Южный',
            'arrival-time' => '7:00',
            'time' => '19ч',
            'price' => '1700'
        ]
    ],
    [
        'alias' => 'Orel',
        [
            'direction' => 'Донецк-Орел',
            'departure-place' => 'АВ Южный',
            'departure-time' => '18:00',
            'departure-days' => 'Вторник, пятница',
            'arrival-place' => 'АВ г. Орел',
            'arrival-time' => '16:00',
            'time' => '22ч',
            'price' => '1800'
        ],
        [
            'direction' => 'Орел-Донецк',
            'departure-place' => 'АВ г. Орел',
            'departure-time' => '8:00',
            'departure-days' => 'Четверг, воскресенье',
            'arrival-place' => 'АВ Южный',
            'arrival-time' => '7:00',
            'time' => '23ч',
            'price' => '1800'
        ]
    ],
    [
        'alias' => 'Belgorod',
        [
            'direction' => 'Донецк-Белгород',
            'departure-place' => 'АВ Южный',
            'departure-time' => '18:00',
            'departure-days' => 'Вторник, пятница',
            'arrival-place' => 'АВ г. Белгород',
            'arrival-time' => '11:00',
            'time' => '17ч',
            'price' => '1500'
        ],
        [
            'direction' => 'Белгород-Донецк',
            'departure-place' => 'АВ г. Белгород',
            'departure-time' => '14:00',
            'departure-days' => 'Четверг, воскресенье',
            'arrival-place' => 'АВ Южный',
            'arrival-time' => '7:00',
            'time' => '16ч',
            'price' => '1500'
        ]
    ],

    [
        'alias' => 'Simferopol',
        [
            'direction' => 'Донецк-Симферополь',
            'departure-place' => 'АС «Центр»',
            'departure-time' => '18:30',
            'departure-days' => 'Вторник, суббота',
            'arrival-place' => 'АВ г.Симферополь',
            'arrival-time' => '15:00',
            'time' => '20ч 30м',
            'price' => '1800'
        ],
        [
            'direction' => 'Симферополь-Донецк',
            'departure-place' => 'АВ г. Симферополь',
            'departure-time' => '11:30',
            'departure-days' => 'Понедельник, четверг',
            'arrival-place' => 'АС «Центр»',
            'arrival-time' => '20:00',
            'time' => '20ч 30мин',
            'price' => '2200'
        ]
    ],
    [
        'alias' => 'Yalta',
        [
            'direction' => 'Донецк-Ялта',
            'departure-place' => 'АС «Центр»',
            'departure-time' => '18:30',
            'departure-days' => 'Вторник, суббота',
            'arrival-place' => 'АВ г.Симферополь',
            'arrival-time' => '16:30',
            'time' => '22ч',
            'price' => '2000'
        ],
        [
            'direction' => 'Ялта-Донецк',
            'departure-place' => 'АВ г. Ялта',
            'departure-time' => '10:00',
            'departure-days' => 'Понедельник, четверг',
            'arrival-place' => 'АС «Центр»',
            'arrival-time' => '20:00',
            'time' => '22ч',
            'price' => '2300'
        ]
    ],
];
?>

<div class="Directions-Wrap JS-Tabs">

    <div class="Directions-Map">
        <?php
        require_once "./img/map.svg"
        ?>
    </div>

    <div class="Directions">
        <section class="Directions-Header">
            <h2>
                <svg class="Routes">
                    <use xlink:href="#routes"></use>
                </svg>
                Наши маршруты
            </h2>
            <p>Нажмите на необходимом городе, чтобы получить подробную информацию по&nbspмаршруту</p>

            <div class="Delivering">
                <button class="JS-Modal-Button" href="#Modal-2">
                    <svg>
                        <use xlink:href="#delivery"></use>
                    </svg>
                    <span>Хочу отправить посылку</span>
                </button>
            </div>
        </section>


        <div class="Directions-Tows">
            <ul class="Direction-List JS-Tabs-Navigation">
                <li class="JS-Tab" data-href="Moscow"><img src="/img/Donetsk-Moscow.jpg" alt=""/>

                    <h3>Донецк-Москва</h3>
                </li>
                <li class="JS-Tab" data-href="Rostov"><img src="/img/Donetsk-Rostov.jpg" alt=""/>

                    <h3>Донецк-Ростов</h3>
                </li>
                <li class="JS-Tab" data-href="Voronezh"><img src="/img/Donetsk-Voronezh.jpg" alt=""/>

                    <h3>Донецк-Воронеж</h3>
                </li>
                <li class="JS-Tab" data-href="Tula"><img src="/img/Donetsk-Tula.jpg" alt=""/>

                    <h3>Донецк-Тула</h3>
                </li>
                <li class="JS-Tab" data-href="Kursk"><img src="/img/Donetsk-Yalta.jpg" alt=""/>

                    <h3>Донецк-Курск</h3>
                </li>
                <li class="JS-Tab" data-href="Orel"><img src="/img/Donetsk-Orel.jpg" alt=""/>

                    <h3>Донецк-Орел</h3>
                </li>
                <li class="JS-Tab" data-href="Belgorod"><img src="/img/Donetsk-Yalta.jpg" alt=""/>

                    <h3>Донецк-Белгород</h3>
                </li>
                <li class="JS-Tab" data-href="Simferopol"><img src="/img/Donetsk-Simferopol.jpg" alt=""/>

                    <h3>Донецк-Симферополь</h3>
                </li>
                <li class="JS-Tab" data-href="Yalta"><img src="/img/Donetsk-Yalta.jpg" alt=""/>

                    <h3>Донецк-Ялта</h3>
                </li>
            </ul>

            <ul class="JS-Tabs-Content Directions-Content">
                <?php foreach ($Data as $item) { ?>
                    <li data-tab='<?= $item['alias'] ?>'>
                        <div class="Ticket JS-Tabs">

                            <div class="Ticket-View" style="background-image:url('/img/Donetsk-<?= $item['alias']?>.jpg')">
                                <button>Назад</button>
                                <span class="Ticket-Title"><?= $item[0]['direction'] ?></span>
                            </div>

                            <ul class="Ticket-Nav JS-Tabs-Navigation">
                                <li>Город отправления:</li>
                                <li class="JS-Tab Active" data-href="Donetsk-<?= $item['alias'] ?>-Tab-1"><?= explode('-',$item[0]['direction'])[0] ?></li>
                                <li class="JS-Tab" data-href="Donetsk-<?= $item['alias'] ?>-Tab-2"><?= explode('-',$item[0]['direction'])[1] ?></li>
                            </ul>

                            <ul class="JS-Tabs-Content">
                                <li data-tab='Donetsk-<?= $item['alias'] ?>-Tab-1'>
                                    <dl>
                                        <dt>Время отправления:</dt>
                                        <dd><?= $item[0]['departure-time'] ?> <?= $item[0]['departure-days'] ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Место отправления:</dt>
                                        <dd><?= $item[0]['departure-place'] ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Время прибытия:</dt>
                                        <dd><?= $item[0]['arrival-time'] ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Место прибытия:</dt>
                                        <dd><?= $item[0]['arrival-place'] ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Время в пути:</dt>
                                        <dd><?= $item[0]['time'] ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Стоимость:</dt>
                                        <dd class="Ticket-Price"><span><?= $item[0]['price'] ?> руб.</span></dd>
                                    </dl>
                                </li>

                                <li data-tab='Donetsk-<?= $item['alias'] ?>-Tab-2'>
                                    <dl>
                                        <dt>Время отправления:</dt>
                                        <dd><?= $item[1]['departure-time'] ?> <?= $item[1]['departure-days'] ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Место отправления:</dt>
                                        <dd><?= $item[1]['departure-place'] ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Время прибытия:</dt>
                                        <dd><?= $item[1]['arrival-time'] ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Место прибытия:</dt>
                                        <dd><?= $item[1]['arrival-place'] ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Время в пути:</dt>
                                        <dd><?= $item[1]['time'] ?></dd>
                                    </dl>
                                    <dl>
                                        <dt>Стоимость:</dt>
                                        <dd class="Ticket-Price"><span><?= $item[1]['price'] ?>р.</span></dd>
                                    </dl>
                                </li>
                            </ul>
                            <button class="Button"><span>Посмотреть маршрут на&nbspЯндекс.Картах</span></button>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>


    </div>

</div>
<section class="Comfort-Trip">
    <h2>Комфортная поездка гарантирована</h2>

    <div class="Trip-Bus">
        <svg>
            <use xlink:href="#bus"></use>
        </svg>
        <span>Чистота в автобусе</span>
    </div>
    <div class="Trip-Driver">
        <svg>
            <use xlink:href="#driver"></use>
        </svg>
        <span>Опытный водитель</span>
    </div>
    <div class="Trip-Border">
        <svg>
            <use xlink:href="#road-sign"></use>
        </svg>
        <span>Без проблем на границе</span>
    </div>

</section>