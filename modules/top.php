
<div class="Header-Group">
    <div class="Header-Wrap">
        <header>
            <div class="Head-Menu">
                <svg class="Logo">
                    <use xlink:href="#logo"></use>
                </svg>
                <div class="Phones-Link JS-Menu-Button">
                    <svg>
                        <use xlink:href="#phone"></use>
                    </svg>
                    <span>Телефоны</span>
                </div>
            </div>

            <div class="Phones-Header JS-Menu-List">
                <div>
                    <dl>
                        <dt>МТС Украина</dt>
                        <dd><a href="tel:(099) 484-73-03">(099) 484-73-03</a></dd>
                    </dl>
                    <dl>
                        <dt>Life:) Украина</dt>
                        <dd><a href="tel:(063) 780-75-26">(063) 780-75-26</a></dd>
                    </dl>
                    <dl>
                        <dt>МТС Россия</dt>
                        <dd><a href="tel:(915) 320-07-25">(915) 320-07-25</a></dd>
                    </dl>
                    <dl>
                        <dt>Феникс ДНР</dt>
                        <dd><a href="tel:(071) 308-13-93">(071) 308-13-93</a></dd>
                    </dl>
                </div>
            </div>

        </header>
    </div>
</div>

<div class="Top">

    <section class="Top-Content">
        <div class="Wrap">
            <h1>Комфортные автобусные маршруты из Донецка
                в Россию</h1>

        </div>
        <div class="Top-Button">
            <button>Просмотреть наши маршруты</button>
        </div>


    </section>
</div>

