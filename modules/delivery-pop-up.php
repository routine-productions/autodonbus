<div class="JS-Modal-Blackout Del-Wrap" id='Modal-2'>
    <div class="Delivery-Wrap JS-Modal">
        <h2>Как отправить посылку?</h2>
        <svg class="Close JS-Modal-Close">
            <use xlink:href="#cross"></use>
        </svg>
        <div class="Delivery">
            <svg>
                <use xlink:href="#delivery"></use>
            </svg>

            <div class="Popup">
                <p>
                    Обратитесь к нашему водителю во время погрузки пассажиров в&nbspместе отправления.
                </p>

                <p>
                    Чтобы узнать время отправления наших автобусов, нажмите на&nbspнеобходимый город.
                </p>
            </div>

        </div>
    </div>
</div>