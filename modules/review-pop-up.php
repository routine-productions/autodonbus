
<div class="JS-Modal-Blackout" id='Modal-1'>
    <form class="Review-Popup JS-Modal JS-Form"
          data-form-email='onevanivan1@gmail.com'
          data-form-subject='Comment form website'
          data-form-url='./js/data.form.php'
          data-form-method='POST'>

        <h2>Оставьте свой отзыв</h2>
        <svg class="Close JS-Modal-Close">
            <use xlink:href="#cross"></use>
        </svg>

        <div><label for="name">Имя</label><input id="name" name='name' data-input-title='Your first name' type="text"/></div>
        <div><label for="town">Город</label><input id="town" name='town' data-input-title='Your first name' type="text"/></div>
        <div><label class="For-Message" for="text">Ваш отзыв</label><textarea id="text" name='message' data-input-title='Your message'></textarea></div>

        <div class="Button JS-Form-Button">
            <button>Отправить отзыв</button>
        </div>
        <div class="JS-Form-Result"></div>

    </form>
</div>
